import Felgo 3.0
import QtQuick 2.0

Page
{
    title: "YouTube"
    backgroundColor: "#ffc8dd"

    YouTubeWebPlayer
    {
        id: youTubePlayer
        videoId: zooPage.currentVideoId
    }
}
