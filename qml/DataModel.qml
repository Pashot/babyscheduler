import QtQuick 2.0
import Felgo 3.0

Item {

    readonly property alias todos: tmpMemory.todos
    readonly property alias todoDetails: tmpMemory.todoDetails

    // action error signals
    signal fetchTodos()

    // listen to actions from dispatcher
    Connections {
        id: logicConnection

        // action 1 - fetchTodos
        onFetchTodos: {
            // check cached value first
            var cached = storage.getValue("todos")
            if(cached)
            {
                tmpMemory.todos = cached
            }
            else
            {
                //cache.setValue("todos",data)
                //tmpMemory.todos = data
            }
        }
    }

    // storage for caching
    Storage {
        id: storage
    }

    // private
    Item {
        id: tmpMemory

        property var todos: []  // Array
        property var todoDetails: ({}) // Map

    }
}
