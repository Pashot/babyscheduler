import Felgo 3.0
import QtQuick 2.0
import QtQuick.Controls 1.0

Page
{
    id: passwordChangingPage
    title: "Смена пароля"
    backgroundColor: "#bde0fe"

    Column
    {
        x: parent.x + 13
        y: parent.y + 13
        width: parent.width -  26


        AppText {
            text: qsTr("Текущий пароль")
            font.pixelSize: sp(12)
        }

        AppTextField {
            id: currentPassword
            showClearButton: true
            width: parent.width
            font.pixelSize: sp(14)
            borderColor: Theme.tintColor
            borderWidth: !Theme.isAndroid ? dp(2) : 0
            echoMode: TextInput.Password
        }

        AppText {
            text: qsTr("Повторите пароль")
            font.pixelSize: sp(12)
        }

        AppTextField {
            id: currentPassword2
            showClearButton: true
            width: parent.width
            font.pixelSize: sp(14)
            borderColor: Theme.tintColor
            borderWidth: !Theme.isAndroid ? dp(2) : 0
            echoMode: TextInput.Password
        }

        AppText {
            text: qsTr("Новый пароль")
            font.pixelSize: sp(12)
        }

        AppTextField {
            id: newPassword
            showClearButton: true
            width: parent.width
            font.pixelSize: sp(14)
            borderColor: Theme.tintColor
            borderWidth: !Theme.isAndroid ? dp(2) : 0
            echoMode: TextInput.Password
        }

        AppButton
        {
            text: "Подтвердить"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked:
            {
                if(currentPassword.text === currentPassword2.text && appSettings.value("password", "") === currentPassword.text)
                {
                    appSettings.setValue("password", newPassword.text);
                    newPassword.clear();
                    currentPassword.clear();
                    currentPassword2.clear();
                }
            }
        }
    }
}
