import Felgo 3.0
import QtQuick 2.0
import Qt.labs.settings 1.0

App {
    onInitTheme:
    {
        Theme.platform = "ios"
        Theme.navigationBar.backgroundColor = "#cdb4db";
        Theme.colors.backgroundColor = "#bde0fe";
    }

    Component.onCompleted:
    {
        dataModel.fetchTodos();
    }

    DataModel
    {
        id: dataModel
    }

    JsonListModel
    {
        id: todoListModel
        //source: dataModel.todos
        source:
        [
            { "id": "a", "todo_image_name": "1.png", "weekday": "Вс", "start_time": 10 * 60 + 00, "end_time": 19 * 60 + 00, "checked": false },
            { "id": "b", "todo_image_name": "2.png", "weekday": "Вс", "start_time":  9 * 60 + 00, "end_time": 10 * 60 + 32, "checked": false },
            { "id": "c", "todo_image_name": "3.png", "weekday": "Вс", "start_time": 10 * 60 + 00, "end_time": 19 * 60 + 00, "checked": false },
            { "id": "d", "todo_image_name": "4.png", "weekday": "Сб", "start_time": 17 * 60 + 00, "end_time": 19 * 60 + 00, "checked": false },
            { "id": "e", "todo_image_name": "5.png", "weekday": "Вс", "start_time": 18 * 60 + 00, "end_time": 19 * 60 + 00, "checked": false },
            { "id": "f", "todo_image_name": "6.png", "weekday": "Сб", "start_time": 18 * 60 + 00, "end_time": 19 * 60 + 00, "checked": false },
            { "id": "g", "todo_image_name": "7.png", "weekday": "Сб", "start_time": 18 * 60 + 00, "end_time": 19 * 60 + 00, "checked": false },
            { "id": "h", "todo_image_name": "8.png", "weekday": "Чт", "start_time": 18 * 60 + 00, "end_time": 19 * 60 + 00, "checked": false }
        ]
        keyField: "id"
        fields: ["id", "todo_image_name", "weekday", "start_time", "end_time", "checked"]
    }

    JsonListModel
    {
        id: zooPageModel
        keyField: "image_name"
        fields: ["image_name", "video_id"]
    }

    JsonListModel
    {
        id: zooPermanentDataModel
        source:
        [
            { "image_name": "dinosaur.png", "video_id": "qpTtMKWNOWo" },
            { "image_name": "cat.png", "video_id": "veqyqoZjKuw" },
            { "image_name": "haski.png", "video_id": "Vsw6MD392iA" },
            { "image_name": "rabbit.png", "video_id": "edKenSaW-es" }
        ]
        keyField: "image_name"
        fields: ["image_name", "video_id"]
    }

    Settings
    {
        id: appSettings
    }

    Navigation {
        id: navigation

        // first tab
        NavigationItem {
            title: qsTr("Todo List")
            icon: IconType.listul

            NavigationStack {
                initialPage: TodoListPage { }
            }
        }

        // second tab
        NavigationItem {
            title: qsTr("Zoo")
            icon: IconType.qq

            NavigationStack {
                initialPage: ZooPage { }
            }
        }

        // third tab
        NavigationItem {
            title: qsTr("Profile")
            icon: IconType.male

            NavigationStack {
                initialPage: PasswordChangingPage { }
            }
        }
    }

    AgreementPage
    {
    }
}
