import Felgo 3.0
import QtQuick 2.0
import QtQuick.Controls 1.0

Page
{
    id: passwordEnteringPage
    title: "Введите пароль"
    backgroundColor: "#bde0fe"

    Column
    {
        x: parent.x + 13
        y: parent.y + 13
        width: parent.width -  26

        AppText {
            text: qsTr("Пароль")
            font.pixelSize: sp(12)
        }

        AppTextField {
            id: password
            showClearButton: true
            width: parent.width
            font.pixelSize: sp(14)
            borderColor: Theme.tintColor
            borderWidth: !Theme.isAndroid ? dp(2) : 0
            echoMode: TextInput.Password
        }

        AppButton
        {
            text: "Подтвердить"
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked:
            {
                if(appSettings.value("password", "") === password.text)
                {
                    passwordEnteringPage.navigationStack.push(todoAddPageComponent)
                }
            }
        }
    }

    Component {
        id: todoAddPageComponent
        TodoAddPage { }
    }
}
