import Felgo 3.0
import QtQuick 2.0

Page {
    id: page
    title: qsTr("Todo List")
    backgroundColor: "#bde0fe"

    rightBarItem: NavigationBarRow
    {
        IconButtonBarItem
        {
            icon: IconType.plus
            showItem: showItemAlways
            onClicked:
            {
                page.navigationStack.popAllExceptFirstAndPush(passwordEnteringComponent)
            }
        }
    }

    AppListView
    {
        id: listView
        anchors.fill: parent
        leftMargin: 10
        rightMargin: 10
        topMargin: 10
        bottomMargin: 10
        backgroundColor: page.backgroundColor

        model: todoListModel

        delegate: SimpleRow {
            id: rowView
            visible: true
            //visible: (model.weekday === Qt.formatDateTime(new Date(), "ddd"))
            width: parent.width
            height: visible ? parent.width / 3 : 0

            Timer {
                id: timer
                interval: 3000
                repeat: true
                running: true

                onTriggered:
                {
                    var currentDate = new Date();
                    if(rowView.visible)
                    {
                        var currentMinutes = 60 * currentDate.getHours() + currentDate.getMinutes();
                        //rowView.enabled = (model.start_time <= currentMinutes && model.end_time > currentMinutes);
                        //rowView.update();
                    }
                }
            }

            property int spacing: 10

            Rectangle
            {
                z: parent.z + 10
                width: parent.width
                height: parent.height - spacing
                color: parent.enabled ? "transparent" : "#33888888"
            }

            Rectangle
            {
                width: parent.width
                height: parent.height
                color: page.backgroundColor

                AppImage
                {
                    source: "../assets/activity/" + model.todo_image_name
                    x: parent.x
                    width: parent.width / 3 - spacing
                    height: parent.height - spacing
                }

                AppImage
                {
                    source: "../assets/" + (model.checked ? "check.png" : "cross.png")
                    x: parent.x + parent.width / 3
                    width: parent.width / 3 - spacing
                    height: parent.height - spacing
                }

                AppImage
                {
                    source: "../assets/" + (model.checked ? "pepe_happy.png" : "pepe_sad.png")
                    x: parent.x + 2 * parent.width / 3
                    width: parent.width / 3 - spacing
                    height: parent.height - spacing
                }
            }


            onSelected:
            {
                var rowId = todoListModel.indexOf("id", model.id)
                var rowData = todoListModel.get(rowId)
                var checkValueCache = rowData["checked"]
                rowData["checked"] = true
                todoListModel.set(rowId, rowData)

                if(!checkValueCache)
                {
                    var allItemsChecked = true;
                    for( var i = 0; i < todoListModel.rowCount(); i++ ) {
                        if(todoListModel.get(i).weekday !== Qt.formatDateTime(new Date(), "ddd"))
                        {
                            continue;
                        }

                        if(!todoListModel.get(i).checked)
                        {
                            allItemsChecked = false;
                            break;
                        }
                    }

                    if(allItemsChecked)
                    {
                        var zooId = Math.floor(Math.random(new Date().getSeconds()) * zooPermanentDataModel.rowCount());
                        var zooRow = zooPermanentDataModel.get(zooId);
                        zooPageModel.append(zooRow);
                    }
                }
            }
        }
    }

    Component {
        id: passwordEnteringComponent
        PasswordEnteringPage { }
    }
}
