import Felgo 3.0
import QtQuick 2.0

Page {
    id: zooPage
    title: qsTr("Zoo")
    backgroundColor: "#ffc8dd"
    property string currentVideoId: ""

    GridView
    {
        x: parent.x + 5
        y: parent.y + 5
        width: parent.width
        height: parent.height
        cellWidth: parent.width / 3
        cellHeight: parent.width / 3

        model: zooPageModel
        delegate: Item
        {
            width: parent.width / 3
            height: parent.width / 3
            MouseArea
            {
                anchors.fill: parent
                Image
                {
                    property int spasing: 5
                    width: parent.width - 2 * spasing
                    height: parent.height - 2 * spasing
                    source: "../assets/zoo/" + model.image_name
                }

                onClicked:
                {
                    zooPage.currentVideoId = model.video_id
                    zooPage.navigationStack.push(youTubePageComponent)
                }
            }
        }
    }

    Component {
        id: youTubePageComponent
        YouTubePage { }
    }
}
