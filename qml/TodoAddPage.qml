import Felgo 3.0
import QtQuick 2.0
import QtQuick.Controls 1.2

Page
{
    id: todoAddPage
    title: qsTr("Add Todo")
    backgroundColor: "#bde0fe"

    property string activityImagePath : "../assets/felgo-logo.png";

    Rectangle
    {
        x: parent.x + 5
        y: parent.y + 5
        width: parent.width - 10
        height: parent.height - 10
        color: todoAddPage.backgroundColor

        Column
        {
            anchors.fill: parent
            spacing: 5

            Rectangle
            {
                width: parent.width
                height: parent.width / 3
                color: todoAddPage.backgroundColor

                MouseArea
                {
                    anchors.centerIn: parent
                    width: parent.width / 3
                    height: parent.width / 3

                    AppImage
                    {
                        width: parent.width
                        height: parent.width
                        source: todoAddPage.activityImagePath
                    }

                    onClicked:
                    {
                        todoAddPage.navigationStack.push(activityPickerPageComponent)
                    }
                }
            }

            ComboBox {
                id: weekdaySelector
                width: parent.width

                model: ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"]
                property var map : { 0: "Пн", 1: "Вт", 2: "Ср", 3: "Чт", 4: "Пт", 5: "Сб", 6: "Вс" }
            }

            Row
            {
                width : parent.width
                height : parent.width / 2
                AppText
                {
                    width : parent.width / 2
                    height : parent.height
                    verticalAlignment: Text.AlignVCenter
                    text: "Start Time: "
                }

                DatePicker
                {
                    x : parent.width / 2
                    width : parent.width / 2
                    height : parent.height
                    id: startTimePicker
                    datePickerMode: timeMode
                }
            }

            Row
            {
                width : parent.width
                height : parent.width / 2
                AppText
                {
                    width : parent.width / 2
                    height : parent.height
                    verticalAlignment: Text.AlignVCenter
                    text: "End Time: "
                }

                DatePicker
                {
                    x : parent.width / 2
                    width : parent.width / 2
                    height : parent.height
                    id: endTimePicker
                    datePickerMode: timeMode
                }
            }

            AppButton
            {
                text: "Подтвердить"
                anchors.horizontalCenter: parent.horizontalCenter
                radius: 3

                function generateUUID(){
                    var d = new Date().getTime();
                    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                        var r = (d + Math.random()*16)%16 | 0;
                        d = Math.floor(d/16);
                        return (c === 'x' ? r : (r&0x3|0x8)).toString(16);
                    });
                    return uuid;
                }

                onClicked:
                {
                    var lastActivitySlashIndex = todoAddPage.activityImagePath.lastIndexOf("/");
                    var activityImageName = todoAddPage.activityImagePath.slice(lastActivitySlashIndex + 1, todoAddPage.activityImagePath.length)

                    todoListModel.append(
                        {
                            "id": generateUUID(),
                            "todo_image_name": activityImageName,
                            "weekday": weekdaySelector.map[weekdaySelector.currentIndex],
                            "start_time": 60 * startTimePicker.selectedDate.getHours() + startTimePicker.selectedDate.getMinutes(),
                            "end_time": 60 * endTimePicker.selectedDate.getHours() + endTimePicker.selectedDate.getMinutes(),
                            "checked": false
                        }
                    )

                    todoAddPage.navigationStack.pop()
                    todoAddPage.navigationStack.pop()
                }
            }
        }
    }

    Component {
        id: activityPickerPageComponent
        ActivityPickerPage { }
    }
}
