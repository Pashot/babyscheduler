import Felgo 3.0
import QtQuick 2.0

Page {
    id: zooPickerPage
    title: qsTr("Zoo Picker")
    backgroundColor: "#bde0fe"

    JsonListModel {
        id: activityPickerModel
        source:
        [
            {"image_name": "1.png"},
            {"image_name": "2.png"},
            {"image_name": "3.png"},
            {"image_name": "4.png"},
            {"image_name": "5.png"},
            {"image_name": "6.png"},
            {"image_name": "7.png"},
            {"image_name": "8.png"}
        ]
        keyField: "image_name"
        fields: ["image_name"]
    }

    GridView
    {
        x: parent.x + 5
        y: parent.y + 5
        width: parent.width
        height: parent.height
        cellWidth: parent.width / 3
        cellHeight: parent.width / 3

        model: activityPickerModel
        delegate: Item
        {
            width: parent.width / 3
            height: parent.width / 3

            MouseArea
            {
                anchors.fill: parent
                Image
                {
                    property int spasing: 5
                    width: parent.width - 2 * spasing
                    height: parent.height - 2 * spasing
                    source: "../assets/activity/" + model.image_name
                }

                onClicked:
                {
                    todoAddPage.activityImagePath = "../assets/activity/" + model.image_name
                    zooPickerPage.navigationStack.pop()
                }
            }
        }
    }
}
